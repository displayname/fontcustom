# FontCustom

## Description
Generate a custom icon-font from a bunch of svgs.

## Install
best way is to follow the instructions at [fontcustom.com](http://fontcustom.com/) for your os.

##Preparing SVG's
[copied from gulp-iconfont](https://github.com/nfroidure/gulp-iconfont)
###Inkscape

Degroup every shapes (Ctrl+Shift+G), convert to pathes (Ctrl+Maj+C) and merge them (Ctrl++). Then save your SVG, prefer 'simple SVG' file type.

###Illustrator

Save your file as SVG with the following settings:

* SVG Profiles: SVG 1.1
* Fonts Type: SVG
* Fonts Subsetting: None
* Options Image Location: Embed
* Advanced Options
   * CSS Properties: Presentation Attributes
   * Decimal Places: 1
   * Encoding: UTF-8
   * Output fewer elements: check

Leave the rest unchecked.

More in-depth information: [http://www.adobe.com/inspire/2013/09/exporting-svg-illustrator.html](http://www.adobe.com/inspire/2013/09/exporting-svg-illustrator.html)

## Alternative
[gulp-iconfont](https://github.com/nfroidure/gulp-iconfont)